﻿using System;
namespace VacuumAuditor
{
    public class Job
    {
        public Guid Id { get; set; }
        public Guid RobotId { get; set; }
        public string RobotName { get; set; }
        public DateTime HappenedAt { get; set; }
    }
}
