﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VacuumAuditor
{
    public class VacuumAuditorDbContext : DbContext
    {

        public DbSet<Job> Jobs { get; set; }

        public VacuumAuditorDbContext(DbContextOptions<VacuumAuditorDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(VacuumAuditorDbContext).Assembly);
        }
    }

    class VacuumCleanerConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.RobotId).IsRequired();
            builder.Property(p => p.RobotName).IsRequired();
            builder.Property(p => p.RobotName).IsRequired().HasMaxLength(256);
            builder.Property(p => p.HappenedAt).IsRequired();
        }
    }

}
