﻿# Vacuum Cleaner History

This is a hipotetical application to centralize the historic usages of a Robotic Vacuum Cleaner and it's maintenance costs.

## Infrastructure requirements

- .Net Core
- SQLite

## Features

- Register vacuum cleaner jobs
    - In each job are received the vacuum id, the vacuum model, the job id, the job time
    - If the vacuum cleaner doesn't exist is created
- Show the jobs by the vacuum cleaner id